// locomotive
const scroll = new LocomotiveScroll({
  el: document.querySelector("#main"),
  smooth: true,
});

function videoConanimation() {
  var videoCon = document.querySelector("#video-con");
  var playBtn = document.querySelector("#play");

  videoCon.addEventListener("mouseenter", function () {
    gsap.to(playBtn, {
      scale: 1,
      opacity: 1,
    });
  });
  videoCon.addEventListener("mouseleave", function () {
    gsap.to(playBtn, {
      scale: 0,
      opacity: 0,
    });
  });
  videoCon.addEventListener("mousemove", function (dets) {
    gsap.to(playBtn, {
      left: dets.x - 40,
      top: dets.y - 40,
    });
  });
}
videoConanimation();

// -----------------------------

function loadingAnimation() {
  gsap.from("#one h1", {
    y: 20,
    opacity: 0,
    delay: 0.5,
    duration: 0.8,
    stagger: 0.3,
  });
  gsap.from("#one #video-con", {
    scale: 0.9,
    opacity: 0,
    delay: 0,
    duration: 0.9,
    stagger: 0.2,
  });
}

loadingAnimation();

